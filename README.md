# Installation du projet

### Commencer par installer [nodejs](https://nodejs.org/en/download) et [docker](https://www.docker.com/get-started/)

#### Allez dans le dossier api est faire `npm install`

#### Allez dans le dossier front est faire `npm install`

#### Revenir à la racine du projet et faire `docker-compose up -d`

Front : [http://localhost:3000](http://localhost:3000) => **[update]** [https://localhost:3000](https://localhost:3000)

Référrez-vous à la partie **"Partie sécurité"** ci-dessous pour plus d'informations

Back : [http://localhost:4000](http://localhost:4000)

PhpMyAdmin: [http://localhost:8080](http://localhost:8080)

MariaDB: [http://localhost:3306](http://localhost:3306)

N'oubliez pas d'importer la bdd qui est dans le dossier (api/import bdd)

### Frameworks

Framework: ReactJS, NodeJS

Pour le css : [taiwindcss](https://tailwindcss.com/docs/installation)

Framework components : [chakra-ui](https://chakra-ui.com/getting-started)

Formulaires : [formik](https://formik.org/docs/api/formik)

Les icones : https://react-icons.github.io/react-icons/icons?name=hi

Enjoy :)

### Partie sécurité 🔒

Le projet est dorénavant en **HTTPS** avec vérification d'un certificat côté client. Si le client ne présente pas avec
un certificat approuvé par l'autorité de certification du serveur, il n'a pas accès à l'application.

Pour installer le certificat dans Firefox :

1. Aller dans **Setttings > Privacy & Security > View Certificates (scroll down to see it)**

2. Dans l'onglet **"Your Certificates"**, cliquer sur **" Import "** et sélectionner le fichier **"SSL
   Features/CLT_cert_firefox.p12"**

3. Tapper le mot de passe ci-dessous et cliquer sur **"Sign in"**

```
MDP : lol1234
```

4. Cliquer sur **"Ok"** et fermer l'onglet

Lors de votre connexion à la page web d'authentification [https://localhost:3000](https://localhost:3000), une pop-up *
*"User Identification Request"** apparaît. Cocher sur **"Remember this decision"** et cliquer sur **"OK"**.

Sélectionner par la suite, toujours sur la page web, **"Advanced"** et **"Accept the risk and continue"**.

La page d'authentification devrait alors apparaître.

#### Ressources 📚

A titre indicatif, concernant la configuration :

* de l'autorité de certification (CA) hébergée sur le serveur
* du certificat du serveur signé par la CA
* du certificat du client signé par la CA

Vous référrez aux commandes ci-dessous :

``` bash
# Create CA for 5 years
openssl req -x509 -new -nodes -key CA_key.key -sha256 -days 1825 -out CA_cert.pem

# Configure the certificate of the server and generate the private key associated (valid for 1 year)
openssl req -new -nodes -out SRV_cert.csr -newkey rsa:4096 -keyout SRV_key.key 

cat > SRV_cert.v3.ext << EOF
authorityKeyIdentifier=keyid,issuer
basicConstraints=CA:FALSE
keyUsage = digitalSignature, nonRepudiation, keyEncipherment, dataEncipherment
subjectAltName = @alt_names

[alt_names]
DNS.1 = RD_test.local
DNS.2 = RD_test_dev.local
IP.1 = 192.168.1.1
EOF

openssl x509 -req -in SRV_cert.csr -CA CA_cert.pem -CAkey CA_key.key -CAcreateserial -out SRV_cert.crt -days 365 -sha256 -extfile SRV_cert.v3.ext

# Configure the certificate of the client and generate the private key associated (valid for 1 year)

openssl req -new -nodes -out CLT_cert.csr -newkey rsa:4096 -keyout CLT_key.key 

openssl x509 -req -in CLT_cert.csr -CA CA_cert.pem -CAkey CA_key.key -CAcreateserial -out CLT_cert.crt -days 365 -sha256

# Read the certificate

openssl x509 -in SRV_cert.crt -noout -text
openssl x509 -in CLT_cert.crt -noout -text
openssl x509 -in CA_cert.pem -noout -text

# Convert certificate in .crt to .p12 in order to import it in web browser

openssl pkcs12 -export -in CLT_cert.crt -inkey CLT_key.key -out CLT_cert_firefox.p12
```

