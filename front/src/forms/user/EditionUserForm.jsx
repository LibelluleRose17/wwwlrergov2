import {
    Button,
    chakra,
    FormControl,
    Input,
    InputGroup,
    InputLeftElement,
    InputRightElement,
    Select,
    Stack,
    useToast
} from "@chakra-ui/react";
import {HELPING, PATIENT} from "../../utils/Role.js";
import {useEffect, useState} from "react";
import useApi from "../../hook/useApi.jsx";
import {useNavigate} from "react-router-dom";
import {useFormik} from "formik";
import {FaLock, FaUserAlt} from "react-icons/fa";
import {getUserLocalStorage} from "../../utils/getUserLocalStorage.js";

const RegisterForm = () => {

    const CFaUserAlt = chakra(FaUserAlt);
    const CFaLock = chakra(FaLock);
    const [showPassword, setShowPassword] = useState(false);
    const handleShowClick = () => setShowPassword(!showPassword);

    const {editionUser, isEditionUser} = useApi();
    const {user} = getUserLocalStorage();
    const toast = useToast();

    const navigate = useNavigate();

    const formSignIn = useFormik({
        initialValues: {
            last_name: user.last_name,
            first_name: user.first_name,
            fk_role_id: user.fk_role_id,
            email: user.email,
            password: user.password
        },
        onSubmit: (values) => {
            editionUser(values, user?.id).then((r) => r);
        }
    });

    useEffect(() => {
        isEditionUser && toast({
            title: isEditionUser ? 'Votre compte à bien était mise à jour' : 'Error',
            status: isEditionUser ? 'success' : 'error',
            duration: 1000,
            isClosable: true,
        })
        setTimeout(() => isEditionUser ? navigate('/dashboard') : "", 2000)
    }, [isEditionUser])

    return <form onSubmit={formSignIn.handleSubmit}>
        <Stack
            spacing={4}
            p="1rem"
            alignItems="center"
        >
            <FormControl>
                <InputGroup size='lg'>
                    <InputLeftElement
                        pointerEvents="none"
                        children={<CFaUserAlt color="pink.300"/>}
                    />
                    <Input
                        type="text"
                        placeholder="nom"
                        name="last_name"
                        onChange={formSignIn.handleChange}
                        value={formSignIn.values?.last_name}
                    />
                </InputGroup>
            </FormControl>
            <FormControl>
                <InputGroup size='lg'>
                    <InputLeftElement
                        pointerEvents="none"
                        children={<CFaUserAlt color="pink.300"/>}
                    />
                    <Input
                        type="text"
                        placeholder="prénom"
                        name="first_name"
                        onChange={formSignIn.handleChange}
                        value={formSignIn.values?.first_name}
                    />
                </InputGroup>
            </FormControl>
            <FormControl>
                <Select size='md' name='fk_role_id' onChange={formSignIn.handleChange}
                        value={formSignIn.values?.fk_role_id}>
                    <option value={PATIENT}>Patient</option>
                    <option value={HELPING}>Aidant</option>
                </Select>
            </FormControl>
            <FormControl>
                <InputGroup size='lg'>
                    <InputLeftElement
                        pointerEvents="none"
                        children={<CFaUserAlt color="pink.300"/>}
                    />
                    <Input
                        type="email"
                        placeholder="email"
                        name="email"
                        onChange={formSignIn.handleChange}
                        value={formSignIn.values?.email}
                    />
                </InputGroup>
            </FormControl>
            <FormControl>
                <InputGroup size='lg'>
                    <InputLeftElement
                        pointerEvents="none"
                        color="gray.300"
                        children={<CFaLock color="pink.300"/>}
                    />
                    <Input
                        type={showPassword ? "text" : "password"}
                        placeholder="Nouveau Password"
                        name="password"
                        required
                        onChange={formSignIn.handleChange}
                        value={formSignIn.values?.password}
                    />
                    <InputRightElement width="4.5rem">
                        <Button h="1.75rem" size="sm" onClick={handleShowClick}>
                            {showPassword ? "Cacher" : "Voir"}
                        </Button>
                    </InputRightElement>
                </InputGroup>
            </FormControl>
            <Button
                borderRadius={0}
                type="submit"
                variant="solid"
                colorScheme="pink"
                width="full"
            >
                Mettre à jour son profil
            </Button>
        </Stack>
    </form>
}

export default RegisterForm;