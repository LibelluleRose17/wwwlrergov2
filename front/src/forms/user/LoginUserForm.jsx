import {
    Button,
    chakra,
    FormControl,
    FormHelperText,
    Image,
    Input,
    InputGroup,
    InputLeftElement,
    InputRightElement,
    Link,
    Stack,
    useToast
} from "@chakra-ui/react";
import {useEffect, useState} from "react";
import useApi from "../../hook/useApi.jsx";
import {useFormik} from "formik";
import {FaLock, FaUserAlt} from "react-icons/fa";
import {useNavigate} from "react-router-dom";

const LoginUserForm = () => {

    const CFaUserAlt = chakra(FaUserAlt);
    const CFaLock = chakra(FaLock);

    const [showPassword, setShowPassword] = useState(false);
    const handleShowClick = () => setShowPassword(!showPassword);

    const toast = useToast();
    const navigate = useNavigate();
    const {login, isLogin} = useApi();

    const form = useFormik({
        initialValues: {
            email: "",
            password: ""
        },
        onSubmit: (values) => {
            login(values).then(r => r);
        }
    });

    useEffect(() => {
        console.log(isLogin);
        isLogin && toast({
            title: isLogin ? 'Vous êtes connecter.' : 'Error',
            description: isLogin ? "votre compte est bien connecter" : "Une erreur c'est produite lors de la connection à votre compte",
            status: isLogin ? 'success' : 'error',
            duration: 1000,
            isClosable: true,
        })
        setTimeout(() => isLogin ? navigate('/dashboard') : "", 1000)
    }, [isLogin])

    return <form onSubmit={form.handleSubmit}>
        <Stack
            spacing={4}
            p="1rem"
            backgroundColor="whiteAlpha.900"
            boxShadow="md"
            alignItems="center"
        >
            <Image
                boxSize='150px'
                align='center'
                objectFit='cover'
                src='./logo.png'
                alt='Dan Abramov'
            />
            <FormControl>
                <InputGroup size='lg'>
                    <InputLeftElement
                        pointerEvents="none"
                        children={<CFaUserAlt color="pink.300"/>}
                    />
                    <Input
                        type="email"
                        placeholder="email"
                        name="email"
                        required
                        onChange={form.handleChange}
                        value={form.values?.email}
                    />
                </InputGroup>
            </FormControl>
            <FormControl>
                <InputGroup size='lg'>
                    <InputLeftElement
                        pointerEvents="none"
                        color="gray.300"
                        children={<CFaLock color="pink.300"/>}
                    />
                    <Input
                        type={showPassword ? "text" : "password"}
                        placeholder="Password"
                        name="password"
                        required
                        onChange={form.handleChange}
                        value={form.values?.password}
                    />
                    <InputRightElement width="4.5rem">
                        <Button h="1.75rem" size="sm" onClick={handleShowClick}>
                            {showPassword ? "Cacher" : "Voir"}
                        </Button>
                    </InputRightElement>
                </InputGroup>
                <FormHelperText textAlign="right" mt='5'>
                    <Link>Mot de passe oublié?</Link>
                </FormHelperText>
            </FormControl>
            <Button
                borderRadius={0}
                type="submit"
                variant="solid"
                colorScheme="pink"
                width="full"
            >
                Se connecter
            </Button>
        </Stack>
    </form>
}

export default LoginUserForm;