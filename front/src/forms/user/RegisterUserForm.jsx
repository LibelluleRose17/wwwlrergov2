import {
    Button,
    chakra,
    FormControl,
    Image,
    Input,
    InputGroup,
    InputLeftElement,
    InputRightElement,
    Select,
    Stack,
    useToast
} from "@chakra-ui/react";
import {HELPING, PATIENT} from "../../utils/Role.js";
import {useEffect, useState} from "react";
import useApi from "../../hook/useApi.jsx";
import {useNavigate} from "react-router-dom";
import {useFormik} from "formik";
import {FaLock, FaUserAlt} from "react-icons/fa";

const RegisterUserForm = () => {

    const CFaUserAlt = chakra(FaUserAlt);
    const CFaLock = chakra(FaLock);
    const [showPassword, setShowPassword] = useState(false);
    const handleShowClick = () => setShowPassword(!showPassword);

    const {register, isRegister} = useApi();
    const toast = useToast();

    const navigate = useNavigate();

    const formSignIn = useFormik({
        initialValues: {
            last_name: "",
            first_name: "",
            fk_role_id: PATIENT,
            email: "",
            password: ""
        },
        onSubmit: (values) => {
            register(values).then((r) => r);
        }
    });

    useEffect(() => {
        isRegister && toast({
            title: isRegister ? 'Votre compte à bien était créer' : 'Error',
            description: isRegister ? "Le compte à bien était créer" : "lors de la création de votre compte",
            status: isRegister ? 'success' : 'error',
            duration: 1000,
            isClosable: true,
        })
        setTimeout(() => isRegister ? navigate('/login') : "", 2000)
    }, [isRegister])

    return <form onSubmit={formSignIn.handleSubmit}>
        <Stack
            spacing={4}
            p="1rem"
            backgroundColor="whiteAlpha.900"
            boxShadow="md"
            alignItems="center"
        >
            <Image
                boxSize='150px'
                align='center'
                objectFit='cover'
                src='./logo.png'
                alt='Dan Abramov'
            />
            <FormControl>
                <InputGroup size='lg'>
                    <InputLeftElement
                        pointerEvents="none"
                        children={<CFaUserAlt color="pink.300"/>}
                    />
                    <Input
                        type="text"
                        placeholder="nom"
                        name="last_name"
                        onChange={formSignIn.handleChange}
                        value={formSignIn.values?.lastName}
                    />
                </InputGroup>
            </FormControl>
            <FormControl>
                <InputGroup size='lg'>
                    <InputLeftElement
                        pointerEvents="none"
                        children={<CFaUserAlt color="pink.300"/>}
                    />
                    <Input
                        type="text"
                        placeholder="prénom"
                        name="first_name"
                        onChange={formSignIn.handleChange}
                        value={formSignIn.values?.firstName}
                    />
                </InputGroup>
            </FormControl>
            <FormControl>
                <Select size='md' name='fk_role_id' onChange={formSignIn.handleChange}
                        value={formSignIn.values?.fk_role_id}>
                    <option value={PATIENT}>Patient</option>
                    <option value={HELPING}>Aidant</option>
                </Select>
            </FormControl>
            <FormControl>
                <InputGroup size='lg'>
                    <InputLeftElement
                        pointerEvents="none"
                        children={<CFaUserAlt color="pink.300"/>}
                    />
                    <Input
                        type="email"
                        placeholder="email"
                        name="email"
                        onChange={formSignIn.handleChange}
                        value={formSignIn.values?.email}
                    />
                </InputGroup>
            </FormControl>
            <FormControl>
                <InputGroup size='lg'>
                    <InputLeftElement
                        pointerEvents="none"
                        color="gray.300"
                        children={<CFaLock color="pink.300"/>}
                    />
                    <Input
                        type={showPassword ? "text" : "password"}
                        placeholder="Password"
                        name="password"
                        onChange={formSignIn.handleChange}
                        value={formSignIn.values?.password}
                    />
                    <InputRightElement width="4.5rem">
                        <Button h="1.75rem" size="sm" onClick={handleShowClick}>
                            {showPassword ? "Cacher" : "Voir"}
                        </Button>
                    </InputRightElement>
                </InputGroup>
            </FormControl>
            <Button
                borderRadius={0}
                type="submit"
                variant="solid"
                colorScheme="pink"
                width="full"
            >
                S'inscrire
            </Button>
        </Stack>
    </form>
}

export default RegisterUserForm;