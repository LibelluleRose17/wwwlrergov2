import * as React from "react";
import * as ReactDOM from "react-dom/client";
import "./index.css";
import {ChakraProvider} from "@chakra-ui/react";
import AppRoutes from "./routes/AppRoutes.jsx";
import {ThemeProvider} from "./theme/ThemeContext.jsx";

ReactDOM.createRoot(document.getElementById("root")).render(
    <ThemeProvider>
        <ChakraProvider>
            <AppRoutes/>
        </ChakraProvider>
    </ThemeProvider>
);