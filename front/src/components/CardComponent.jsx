import {Badge, Button, Card, CardBody, CardFooter, CardHeader, Heading, Text} from "@chakra-ui/react";
import {useThemeContext} from "../theme/ThemeContext.jsx";

const CardComponent = ({count, title, description, icon}) => {

    const {backgroundPrimary, backgroundSecondary} = useThemeContext();

    return <Card
        className={`transition ease-in-out delay-150 ${backgroundPrimary} hover:${backgroundSecondary} text-white`}>
        <CardHeader>
            {icon}
            <Heading size='lg'><Badge className='p-3 rounded mr-3 text-xl'>{count}</Badge>{title}</Heading>
        </CardHeader>
        <CardBody>
            <Text>{description}</Text>
        </CardBody>
        <CardFooter>
            <Button className='bg-black'>View here</Button>
        </CardFooter>
    </Card>;
}

export default CardComponent