import {Link, useNavigate} from "react-router-dom";
import {HiOutlineBan, HiOutlineUser, HiStatusOnline, HiViewGrid} from "react-icons/hi";
import {useThemeContext} from "../theme/ThemeContext.jsx";

const Menu = ({pathname}) => {
    const {backgroundPrimary, backgroundSecondary} = useThemeContext();

    const navigate = useNavigate();

    return <>
        <div id="sidebar">
            <img className='mt-10' alt={'logo'} src={'logo.png'} onClick={() => navigate('/dashboard')}/>
            <div className='divide-y'></div>
            <nav>
                <ul>
                    <li className={`${pathname === '/dashboard' ? `rounded ${backgroundSecondary} text-white` : `rounded ${backgroundPrimary} text-white`}`}>
                        <Link to={`dashboard`}>
                            <HiViewGrid/> Dashboard
                        </Link>
                    </li>
                    <li className={`${pathname === '/sensors' ? `rounded ${backgroundSecondary} text-white` : `rounded ${backgroundPrimary} text-white`}`}>
                        <Link to={`sensors`}>
                            <HiStatusOnline/> Capteurs
                        </Link>
                    </li>
                    <div className='divide-y'></div>
                    <li className={`${pathname === '/profil' ? `rounded ${backgroundSecondary} text-white` : `rounded ${backgroundPrimary} text-white`}`}>
                        <Link to={`profil`}>
                            <HiOutlineUser/> Profil
                        </Link>
                    </li>
                    <li className='absolute bottom-5'>
                        <Link to={`/`} onClick={() => localStorage.clear()}>
                            <HiOutlineBan/> Se déconnecter
                        </Link>
                    </li>
                </ul>
            </nav>
        </div>
    </>;
}

export default Menu;