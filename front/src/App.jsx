import {Outlet, useLocation} from "react-router-dom";
import Menu from "./components/Menu.jsx";


const App = () => {

    const location = useLocation();
    const {pathname} = location;

    return (
        <>
            <Menu pathname={pathname}/>
            <div id="outlet">
                <Outlet/>
            </div>
        </>
    );
}

export default App
