import {Box, Flex, Link, Stack} from "@chakra-ui/react";
import LoginUserForm from "../forms/user/LoginUserForm.jsx";

const Login = () =>
    (
        <Flex
            flexDirection="column"
            width="100%"
            height="100vh"
            backgroundColor="gray.200"
            justifyContent="center"
            alignItems="center"
        >
            <Stack
                flexDir="column"
                mb="2"
                justifyContent="center"
                alignItems="center"
            >
                <Box minW={{base: "90%", md: "468px"}}>
                    <LoginUserForm/>
                </Box>
            </Stack>
            <Box>
                Premiére connexion?{" "}
                <Link color="pink.500" href="/register">
                    S'inscrire
                </Link>
            </Box>
        </Flex>
    );

export default Login;
