import {Center, Divider, SimpleGrid} from "@chakra-ui/react";
import CardComponent from "../components/CardComponent.jsx";
import {HiOutlineUserGroup, HiStatusOnline} from "react-icons/hi";
import BadgeUser from "../components/BadgeUser.jsx";

export const Dashboard = () => {

    return (
        <>
            <BadgeUser/>
            <div className='w-100'>
                <Center height='50px'>
                    <Divider/>
                </Center>
                <SimpleGrid spacing={4} templateColumns='repeat(auto-fill, minmax(400px, 1fr))'>
                    <CardComponent
                        count={23}
                        title={'aidants'}
                        description={'Retrouver le nombre de patients'}
                        icon={<HiOutlineUserGroup size={'15em'} className='absolute right-5 opacity-20'/>}/>
                    <CardComponent
                        count={5}
                        title={'capteurs'}
                        description={'Retrouver le nombre de capteurs'}
                        icon={<HiStatusOnline size={'15em'} className='absolute right-5 opacity-20'/>}/>
                </SimpleGrid>
            </div>
        </>
    );
}

export default Dashboard;