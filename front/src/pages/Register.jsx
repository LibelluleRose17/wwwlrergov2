import {Box, chakra, Flex, Link, Stack} from "@chakra-ui/react";
import {FaLock, FaUserAlt} from "react-icons/fa";
import RegisterUserForm from "../forms/user/RegisterUserForm.jsx";

const CFaUserAlt = chakra(FaUserAlt);
const CFaLock = chakra(FaLock);

const Register = () =>
    (
        <Flex
            flexDirection="column"
            width="100%"
            height="100vh"
            backgroundColor="gray.200"
            justifyContent="center"
            alignItems="center"
        >
            <Stack
                flexDir="column"
                mb="2"
                justifyContent="center"
                alignItems="center"
            >
                <Box minW={{base: "90%", md: "468px"}}>
                    <RegisterUserForm/>
                </Box>
            </Stack>
            <Box>
                Déjà inscrit ?{" "}
                <Link color="pink.500" href="/">
                    Se connecter
                </Link>
            </Box>
        </Flex>
    );

export default Register;
