import {useEffect} from "react";
import {Tab, TabList, TabPanel, TabPanels, Tabs} from "@chakra-ui/react";
import Stepper from "../components/StepperCustom.jsx";
import StepperCustom from "../components/StepperCustom.jsx";

export const Sensors = () => {

    useEffect(() => {
        fetch('http://localhost:4000').then((response) => response.json()).then((data) => console.log(data) )
    }, [])

    return (
        <Tabs>
            <TabList>
                <Tab>Ajouter un capteur</Tab>
                <Tab>Mes capteurs</Tab>
            </TabList>

            <TabPanels>
                <TabPanel>
                    <StepperCustom/>
                </TabPanel>
                <TabPanel>
                    <p>Mes capteurs</p>
                </TabPanel>
            </TabPanels>
        </Tabs>
    );
}

export default Sensors;