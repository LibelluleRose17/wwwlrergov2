import {Navigate, Outlet} from "react-router-dom";

const PrivateRoutes = () => {
    const userStorage = localStorage.getItem('user');

    return userStorage ? <Outlet/> : <Navigate to={'/login'}/>
}

export default PrivateRoutes;