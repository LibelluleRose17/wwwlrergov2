import {BrowserRouter, Navigate, Route, Routes} from "react-router-dom";
import App from "../App.jsx";
import Dashboard from "../pages/Dashboard.jsx";
import Sensors from "../pages/Sensors.jsx";
import Profil from "../pages/Profil.jsx";
import Login from "../pages/Login.jsx";
import Register from "../pages/Register.jsx";
import * as React from "react";
import PrivateRoutes from "./PrivateRoutes.jsx";

const AppRoutes = () =>
    <BrowserRouter>
        <Routes>
            <Route element={<PrivateRoutes/>}>
                <Route path='/' element={<Navigate to='login' replace/>}/>
                <Route
                    path="/"
                    element={<App/>}
                >
                    <Route path="dashboard" element={<Dashboard/>}/>
                    <Route path="sensors" element={<Sensors/>}/>
                    <Route path="profil" element={<Profil/>}/>
                </Route>
            </Route>
            <Route path="/login" element={<Login/>}/>
            <Route path="/register" element={<Register/>}/>
        </Routes>
    </BrowserRouter>

export default AppRoutes;