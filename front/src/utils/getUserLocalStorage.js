import {useMemo, useState} from "react";

export const getUserLocalStorage = () => {
    const getUser = JSON.parse(localStorage.getItem('user'));

    const [user, setUser] = useState(getUser);

    useMemo(() => {
        setUser(getUser);
    }, []);

    return {
        user
    }
}