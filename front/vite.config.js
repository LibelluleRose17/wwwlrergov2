import {defineConfig} from 'vite'
import react from '@vitejs/plugin-react'
import fs from 'fs'
import 'dotenv/config'

const cacheDir =
    process.env.NODE_ENV === 'development-docker'
        ? '/app/node_modules/.vite'
        : 'node_modules/.vite';

export default defineConfig({
    plugins: [react()],
    server: {
        host: true,
        port: process.env.REACT_APP_PORT_WEB_SERVICE,
        https: {
            key: fs.readFileSync(process.env.REACT_APP_SERVER_KEY),
            cert: fs.readFileSync(process.env.REACT_APP_SERVER_CRT),
            ca: fs.readFileSync(process.env.REACT_APP_CA),
            requestCert: true,
            rejectUnauthorized: true,
        },
        fs: {
            strict: true,
            deny: ['.env', '.env.*', '*.{crt,pem,key,csr,ext,srl,p12}', '.*', '*.json', '*.config.js', '*.{doc,docx,xls,xlsx,ppt,pptx,pdf,md}']
        },
    },
    cacheDir,
})
