const express = require('express');
const router = express.Router();
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const db = require('../../dbConnection');
const {signupValidation, loginValidation} = require("../../validations");


// REGISTER USER
router.post('/register', signupValidation, (req, res, next) => {
    db.query(
        `SELECT *
         FROM users
         WHERE LOWER(email) = LOWER(${db.escape(
                 req.body.email
         )});`,

        (err, result) => {
            if (result.length) {
                return res.status(409).send({
                    msg: 'Le compte existe deja'
                });
            } else {
                // username is available
                bcrypt.hash(req.body.password, 10, (err, hash) => {
                    if (err) {
                        return res.status(500).send({
                            msg: err
                        });
                    } else {
                        // has hashed pw => add to database
                        db.query(
                            `INSERT INTO users (fk_role_id, first_name, last_name, email, password)
                             VALUES ('${req.body.fk_role_id}', '${req.body.first_name}', '${req.body.last_name}',
                                     '${req.body.email}', '${hash}')`,
                            (err) => {
                                if (err) {
                                    return res.status(400).send({
                                        msg: err
                                    });
                                }
                                return res.status(201).send({
                                    msg: "L'utilisateur à bien était créer",
                                    create: true
                                });
                            }
                        );
                    }
                });
            }
        }
    );
});


router.post('/edit/:id', signupValidation, (req, res, next) => {
    const id = req.params.id;
    const updateData = req.body;
    const sql = `UPDATE users
                 SET ?
                 WHERE id = ?`;

    bcrypt.hash(req.body.password, 10, (err, hash) => {
        db.query(sql, [{...updateData, password: hash}, id], () => {
            return res.status(200).send({
                msg: 'Votre compte à bien était mise à jour !',
                user: {...req.body, id: id},
                update: true
            });

        });
    })
});

router.get('/user/:id', (req, res, next) => {
    const id = req.params.id;
    const sql = `SELECT *
                 FROM users
                 WHERE id = ${id}`;

    db.query(sql, (err, result) => {
        return res.status(200).send({
            user: result[0],
        });
    });
});


// USER LOGIN
router.post('/login', loginValidation, (req, res, next) => {
    db.query(
        `SELECT *
         FROM users
         WHERE email = ${db.escape(req.body.email)};`,
        (err, result) => {
            // user does not exists
            if (err) {
                return res.status(400).send({
                    msg: err
                });
            }
            if (!result.length) {
                return res.status(401).send({
                    msg: 'Email et/ou password sont incorrect !'
                });
            }
            // check password
            bcrypt.compare(
                req.body.password,
                result[0]['password'],
                (bErr, bResult) => {
                    // wrong password
                    if (bErr) {
                        return res.status(401).send({
                            msg: 'Email et/ou password sont incorrect !'
                        });
                    }
                    if (bResult) {
                        const token = jwt.sign({id: result[0].id}, 'the-super-strong-secrect', {expiresIn: '1h'});

                        const {password, ...rest} = result[0];

                        return res.status(200).send({
                            msg: 'Logged in!',
                            login: true,
                            token,
                            user: rest
                        });
                    }
                    return res.status(401).send({
                        msg: 'Email et/ou password sont incorrect !'
                    });
                }
            );
        }
    );
});

module.exports = router;