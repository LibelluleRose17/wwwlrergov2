-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Hôte : db
-- Généré le : mar. 27 juin 2023 à 08:38
-- Version du serveur : 10.9.3-MariaDB-1:10.9.3+maria~ubu2204
-- Version de PHP : 8.1.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `libelluleErgo`
--

-- --------------------------------------------------------

--
-- Structure de la table `patient_assist`
--

CREATE TABLE `patient_assist` (
  `id` int(11) NOT NULL,
  `fk_patient_id` int(11) NOT NULL,
  `fk_assist_id` int(11) NOT NULL,
  `start_date` date NOT NULL DEFAULT current_timestamp(),
  `end_date` date DEFAULT NULL,
  `blocked` tinyint(1) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `patient_sensor`
--

CREATE TABLE `patient_sensor` (
  `id` int(11) NOT NULL,
  `fk_users_id` int(11) NOT NULL,
  `fk_sensor_id` int(11) NOT NULL,
  `settings` varchar(255) DEFAULT NULL,
  `start_date` date NOT NULL DEFAULT current_timestamp(),
  `end_date` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `role`
--

CREATE TABLE `role` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `role`
--

INSERT INTO `role` (`id`, `name`) VALUES
(1, 'patient'),
(2, 'assist');

-- --------------------------------------------------------

--
-- Structure de la table `role_straight`
--

CREATE TABLE `role_straight` (
  `id` int(11) NOT NULL,
  `fk_role_id` int(11) NOT NULL,
  `fk_straight_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `sensor`
--

CREATE TABLE `sensor` (
  `id` int(11) NOT NULL,
  `fk_sensor_status_id` int(11) NOT NULL,
  `fk_sensor_type_id` int(11) NOT NULL,
  `size` int(11) NOT NULL,
  `settings` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `sensor_status`
--

CREATE TABLE `sensor_status` (
  `id` int(11) NOT NULL,
  `status` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `sensor_type`
--

CREATE TABLE `sensor_type` (
  `id` int(11) NOT NULL,
  `type` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `straight`
--

CREATE TABLE `straight` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `straight_create` tinyint(1) DEFAULT NULL,
  `straight_read` tinyint(1) DEFAULT NULL,
  `straight_update` tinyint(1) DEFAULT NULL,
  `straight_delete` tinyint(1) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `fk_role_id` int(11) NOT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `first_name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `pseudo` varchar(255) DEFAULT NULL,
  `avatar` varchar(255) DEFAULT NULL COMMENT 'avatar link'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `version`
--

CREATE TABLE `version` (
  `id` int(11) NOT NULL,
  `version` varchar(255) DEFAULT NULL,
  `installation_date` timestamp NOT NULL DEFAULT current_timestamp(),
  `description` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `patient_assist`
--
ALTER TABLE `patient_assist`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_patient_id` (`fk_patient_id`),
  ADD KEY `fk_assist_id` (`fk_assist_id`);

--
-- Index pour la table `patient_sensor`
--
ALTER TABLE `patient_sensor`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_users_id` (`fk_users_id`),
  ADD KEY `fk_sensor_id` (`fk_sensor_id`);

--
-- Index pour la table `role`
--
ALTER TABLE `role`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `role_straight`
--
ALTER TABLE `role_straight`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_role_id` (`fk_role_id`),
  ADD KEY `fk_straight_id` (`fk_straight_id`);

--
-- Index pour la table `sensor`
--
ALTER TABLE `sensor`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_sensor_status_id` (`fk_sensor_status_id`),
  ADD KEY `fk_sensor_type_id` (`fk_sensor_type_id`);

--
-- Index pour la table `sensor_status`
--
ALTER TABLE `sensor_status`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `sensor_type`
--
ALTER TABLE `sensor_type`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `straight`
--
ALTER TABLE `straight`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_role_id` (`fk_role_id`);

--
-- Index pour la table `version`
--
ALTER TABLE `version`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `patient_assist`
--
ALTER TABLE `patient_assist`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `patient_sensor`
--
ALTER TABLE `patient_sensor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `role`
--
ALTER TABLE `role`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table `role_straight`
--
ALTER TABLE `role_straight`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `sensor`
--
ALTER TABLE `sensor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `sensor_status`
--
ALTER TABLE `sensor_status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `sensor_type`
--
ALTER TABLE `sensor_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `straight`
--
ALTER TABLE `straight`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table `version`
--
ALTER TABLE `version`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `patient_assist`
--
ALTER TABLE `patient_assist`
  ADD CONSTRAINT `patient_assist_ibfk_1` FOREIGN KEY (`fk_patient_id`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `patient_assist_ibfk_2` FOREIGN KEY (`fk_assist_id`) REFERENCES `users` (`id`);

--
-- Contraintes pour la table `patient_sensor`
--
ALTER TABLE `patient_sensor`
  ADD CONSTRAINT `patient_sensor_ibfk_1` FOREIGN KEY (`fk_users_id`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `patient_sensor_ibfk_2` FOREIGN KEY (`fk_sensor_id`) REFERENCES `sensor` (`id`);

--
-- Contraintes pour la table `role_straight`
--
ALTER TABLE `role_straight`
  ADD CONSTRAINT `role_straight_ibfk_1` FOREIGN KEY (`fk_role_id`) REFERENCES `role` (`id`),
  ADD CONSTRAINT `role_straight_ibfk_2` FOREIGN KEY (`fk_straight_id`) REFERENCES `straight` (`id`);

--
-- Contraintes pour la table `sensor`
--
ALTER TABLE `sensor`
  ADD CONSTRAINT `sensor_ibfk_1` FOREIGN KEY (`fk_sensor_status_id`) REFERENCES `sensor_status` (`id`),
  ADD CONSTRAINT `sensor_ibfk_2` FOREIGN KEY (`fk_sensor_type_id`) REFERENCES `sensor_type` (`id`);

--
-- Contraintes pour la table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_ibfk_1` FOREIGN KEY (`fk_role_id`) REFERENCES `role` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
